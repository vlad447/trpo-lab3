﻿using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("R=");
            String StringR = Console.ReadLine();
            double R = Convert.ToDouble(StringR);

            Console.WriteLine("h=");
            String Stringh = Console.ReadLine();
            double h = Convert.ToDouble(Stringh);

            var S = Functions.SharSegment(R, h);
            Console.WriteLine($"S={S}") ;
        }
    }
}
