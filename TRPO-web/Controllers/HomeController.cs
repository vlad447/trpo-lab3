﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TRPO_web.Models;
using TRPO_Lab3.Lib;

namespace TRPO_web.Controllers
{
    public class HomeController : Controller
    {
       [HttpGet]
       public IActionResult Index()
        {
            return View();
        }
       [HttpPost]
       public IActionResult Index(double R, double h)
        {
            double S = Functions.SharSegment(h, R);
            ViewBag.result = S;
            return View();
        }

    }
}
